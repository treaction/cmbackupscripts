# create_appfiles_backup.sh
# HOST: 62.138.184.191, PATH: /var/www/vhosts/campaign-treaction.de/httpdocs/cmbackup/sh/create_appfiles_backup.sh

DATE=`/usr/bin/date +%Y-%m-%d`
TIMESTAMP=`/usr/bin/date +%Y-%m-%d-%H:%M`
SAVEPATH=/var/www/vhosts/campaign-treaction.de/cmbackup/app
ROOTPATH=/var/www/vhosts/campaign-treaction.de/httpdocs

## SECTION 1
## Prepare log file
if /usr/bin/test -f "$SAVEPATH/app-backup.log"; then
  # overwrite existing, old app-backup.log
  /usr/bin/echo "DUMP LOG DATE: $DATE,  TIMESTAMP: $TIMESTAMP" >$SAVEPATH/app-backup.log
else
  # create app-backup.log
  /usr/bin/touch $MYPATH/app-backup.log
fi


## SECTION 2
## do daily backups and cleanup outdated backup-storage files
# backup system directory
for FILE in `/usr/bin/find $ROOTPATH/system -newermt $(date +%Y-%m-%d -d '10 day ago') -type f -print`
do
  # TODO: change file name
	/usr/bin/tar -uf "$SAVEPATH/$DATE-backup-system.tar"  $FILE
done

# backup Uploads directory
for FILE in `/usr/bin/find $ROOTPATH/Uploads -newermt $(date +%Y-%m-%d -d '10 day ago') -type f -print`
do
  # TODO: change file name
	/usr/bin/tar -uf "$SAVEPATH/$DATE-backup-Uploads.tar"  $FILE
done

# backup all changed php files of last day
for FILE in `/usr/bin/find $ROOTPATH/ -newermt $(date +%Y-%m-%d -d '1 day ago') -name *.php -print`;
do
  /usr/bin/tar -uf "$SAVEPATH/$DATE-backup-changed-php.tar"  $FILE
done


# zip all tar-files before rsync
/usr/bin/zip "$SAVEPATH/$DATE-backup-system.tar.zip" "$SAVEPATH/$DATE-backup-system.tar"
/usr/bin/zip "$SAVEPATH/$DATE-backup-Uploads.tar.zip" "$SAVEPATH/$DATE-backup-Uploads.tar"
if /usr/bin/test -f "$SAVEPATH/$DATE-backup-changed-php.tar"; then
  /usr/bin/zip "$SAVEPATH/$DATE-backup-changed-php.tar.zip" "$SAVEPATH/$DATE-backup-changed-php.tar"
fi


# copy system-dir-backup to backup storage
/usr/bin/rsync -auvz -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem" $SAVEPATH/$DATE-backup-system.tar.zip ftp13750766-neu@wp13750766.server-he.de:appfiles  &>>$SAVEPATH/app-backup.log

# copy Uploads-dir-backup to backup storage
/usr/bin/rsync -auvz -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem" $SAVEPATH/$DATE-backup-Uploads.tar.zip ftp13750766-neu@wp13750766.server-he.de:appfiles  &>>$SAVEPATH/app-backup.log

# copy changed php files of yesterday
if /usr/bin/test -f "$SAVEPATH/$DATE-backup-changed-php.tar"; then
  /usr/bin/rsync -auvz -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem" $SAVEPATH/$DATE-backup-changed-php.tar.zip ftp13750766-neu@wp13750766.server-he.de:appfiles  &>>$SAVEPATH/app-backup.log
fi

# delete old remote files
# remove older-than-3-month tar file from remote server, which contains filesystem-content from previous day + 4 hours past midnight. MONTH3AGO=YYYY-MM-DD -> 30 days before current date.
MONTH3AGO=$(/usr/bin/date --date="${dataset_date} -3 month" +%Y-%m-%d)
# if not first of month delete the 30-days-ago file
/usr/bin/rsync -rv -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem"  --delete --include="$MONTH3AGO-backup-system.tar.zip" '--exclude=*' /var/www/vhosts/campaign-treaction.de/cmbackup/app/ ftp13750766-neu@wp13750766.server-he.de:appfiles &>>$SAVEPATH/app-backup.log
/usr/bin/rsync -rv -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem"  --delete --include="$MONTH3AGO-backup-Uploads.tar.zip" '--exclude=*' /var/www/vhosts/campaign-treaction.de/cmbackup/app/ ftp13750766-neu@wp13750766.server-he.de:appfiles &>>$SAVEPATH/app-backup.log
/usr/bin/rsync -rv -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem"  --delete --include="$MONTH3AGO-backup-changed-php.tar.zip" '--exclude=*' /var/www/vhosts/campaign-treaction.de/cmbackup/app/ ftp13750766-neu@wp13750766.server-he.de:appfiles &>>$SAVEPATH/app-backup.log

# delete local tar and tar.zip files
rm -f $SAVEPATH/$DATE-backup-system.tar $SAVEPATH/$DATE-backup-system.tar.zip
rm -f $SAVEPATH/$DATE-backup-Uploads.tar $SAVEPATH/$DATE-backup-Uploads.tar.zip
if /usr/bin/test -f "$SAVEPATH/$DATE-backup-changed-php.tar"; then
  rm -f $SAVEPATH/$DATE-changed-php.tar $SAVEPATH/$DATE-changed-php.tar.zip
fi

## SECTION 3
## do monthly full-backup 4h after the end of each month at the 1st of new month
if [[ "$DATE" =~ ^20[0-9]{2}\-[0-9]{2}\-01 ]]; then
  MONTH3AGO=$(/usr/bin/date --date="${dataset_date} -3 month" +%Y-%m)-01
  echo "-----------------------------------" &>>$SAVEPATH/app-backup.log
  echo "MONTHLY FULL BACKUP AT $DATE" &>>$SAVEPATH/app-backup.log
  echo "-----------------------------------" &>>$SAVEPATH/app-backup.log

  for FILE in `/usr/bin/find $ROOTPATH/ * -print`
  do
    /usr/bin/tar -uf "$SAVEPATH/$DATE-backup-full.tar"  $FILE
  done

  # zip *-backup-full.tar file
  /usr/bin/zip "$SAVEPATH/$DATE-backup-full.tar.zip" "$SAVEPATH/$DATE-backup-full.tar"

  # copy *backup-full.tar.zip to backup-storage
  /usr/bin/rsync -auvz -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem" $SAVEPATH/$DATE-backup-full.tar.zip ftp13750766-neu@wp13750766.server-he.de:appfiles  &>>$SAVEPATH/app-backup.log

  # delete $MONTH3AGO-backup-full.tar.zip from backup-storage
  /usr/bin/rsync -rv -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem"  --delete --include="$MONTH3AGO-backup-full.tar.zip" '--exclude=*' /var/www/vhosts/campaign-treaction.de/cmbackup/app/ ftp13750766-neu@wp13750766.server-he.de:appfiles &>>$SAVEPATH/app-backup.log

  # delete local tar and tar.zip file
  rm -f $SAVEPATH/$DATE-backup-full.tar $SAVEPATH/$DATE-backup-full.tar.zip
fi

