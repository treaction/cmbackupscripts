# create_db_dumps.sh
# HOST: 62.138.184.191, PATH: /var/www/vhosts/campaign-treaction.de/httpdocs/cmbackup/sh/create_db_dumps.sh
declare -a  databases=("4wave_maas_db" "abmelder_ems" "apsc" "blacklist" "ceoo_maas_db" "effectmail_db" "efox_db" "gianix_db" "giganews_db" "lead_mania_db" "leadworld_maas_db" "living_voucher_db" "peppmt" "psm_db" "rabatt_riese_db" "shopping_guru_db" "stellar_maas_db" "taglich_angebote_db" "verntex_db")
### TEST: declare -a  databases=("apsc" "peppmt")
DATE=`/usr/bin/date +%Y-%m-%d`
TIMESTAMP=`/usr/bin/date +%Y-%m-%d-%H:%M`
MYPATH=/var/www/vhosts/campaign-treaction.de/cmbackup/db

# create new dump.log file for each new dump run
# only errors will be logged to the dump.log
if /usr/bin/test -f "$MYPATH/dump.log"; then
  # overwrite existing, old dump.log
  /usr/bin/echo "DUMP LOG DATE: $DATE,  TIMESTAMP: $TIMESTAMP" >$MYPATH/dump.log
else
  # create dump.log
  /usr/bin/touch $MYPATH/dump.log
fi


# loop over all 19 databases
for DBNAME in "${databases[@]}"
do
  #  create dump
  /usr/bin/mysqldump -u crondump -p'aebi0mohp5IeNgee6oi' --databases "$DBNAME" >"$MYPATH/$DATE-dump-$DBNAME.sql"

  # create zip files
  /usr/bin/zip "$MYPATH/$DATE-dump-$DBNAME.sql.zip" "$MYPATH/$DATE-dump-$DBNAME.sql"

  # delete local dump files (*.sql) after zip files are created
  /usr/bin/rm -f "$MYPATH/$DATE-dump-$DBNAME.sql"
done

#  tar all zip files
/usr/bin/tar -cf "$MYPATH/$DATE-dump-all-cm-databases.tar" $MYPATH/$DATE*zip  &>>$MYPATH/dump.log

# copy the tar file to remote backup server
# scp -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem "$MYPATH/$DATE-dump-all-cm-databases.tar" ftp13750766-neu@wp13750766.server-he.de:dbdumps
/usr/bin/rsync -auvz -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem" $MYPATH/$DATE-dump-all-cm-databases.tar ftp13750766-neu@wp13750766.server-he.de:dbdumps  &>>$MYPATH/dump.log

# delete all local zip files
/usr/bin/rm -f $MYPATH/$DATE*.zip  &>>$MYPATH/dump.log

# delete local tar file
/usr/bin/rm -f "$MYPATH/$DATE-dump-all-cm-databases.tar"  &>>$MYPATH/dump.log

# remove all older-than-30-days tar files from remote server except the first file of month, which contains dump-file-content from previous day + 4 hours past midnight. DAYS30AGO=YYYY-MM-DD -> 30 days before current date.
DAYS30AGO=$(/usr/bin/date --date="${dataset_date} -30 day" +%Y-%m-%d)
if [[ "$DAYS30AGO" =~ ^20[0-9]{2}\-[0-9]{2}\-01 ]]; then
# if first of month DO NOT delete the 30-days-ago file
    /usr/bin/echo "leave out deletion of first of month $DAYS30AGO dump tar-file" &>>$MYPATH/dump.log
else
# if not first of month delete the 30-days-ago file
  /usr/bin/rsync -rv -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem"  --delete --include="$DAYS30AGO-dump-all-cm-databases.tar" '--exclude=*' /var/www/vhosts/campaign-treaction.de/cmbackup/db/ ftp13750766-neu@wp13750766.server-he.de:dbdumps &>>$MYPATH/dump.log
fi

# remove older-than-12-month monthly db-dump-backup-file (1st of month) from backup-storage
if [[ "$DATE" =~ ^20[0-9]{2}\-[0-9]{2}\-01 ]]; then
 MONTH12AGO=$(/usr/bin/date --date="${dataset_date} -12 month" +%Y-%m)-01
 /usr/bin/rsync -rv -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem"  --delete --include="$MONTH12AGO-dump-all-cm-databases.tar" '--exclude=*' /var/www/vhosts/campaign-treaction.de/cmbackup/db/ ftp13750766-neu@wp13750766.server-he.de:dbdumps &>>$MYPATH/dump.log
fi



