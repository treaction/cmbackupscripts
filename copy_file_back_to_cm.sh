# on receiving system: cd into target path

# change filename here:
FILENAME=2021-12-04-dump-all-cm-databases.tar

# change target-filename here:
TARGETFILENAME=deleteme.tar

# copy the file to target-path / target-filename
rsync -chavzP -e "ssh -i /var/www/vhosts/campaign-treaction.de/.ssh/wp13750766_private_key.pem" ftp13750766-neu@wp13750766.server-he.de:dbdumps/$FILENAME ./$TARGETFILENAME

# change owner and group
chown admin_test_ssh:psacln ./$TARGETFILENAME

# additional to do for you:
# if necessary change file permissions!!!!

echo DONE!
